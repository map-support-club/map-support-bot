const deleteMessage = require('../../utils/deleteMessage')

async function announce({ bot, message, context }) {
  const roomID = message.rid

  // Get the announcement by removing "PREFIX COMMAND " from the message
  const announcement = message.msg.substring(
    `${context.prefix} ${context.commandName} `.length,
  )

  // Delete the user's message
  await deleteMessage(roomID, message._id)

  // Post the message in the room
  await bot.sendToRoom(announcement, roomID,)
  return true
}

module.exports = {
  description:
    "Have the bot post a message to the room where the command is called",
  help: `${process.env.ROCKETCHAT_PREFIX} announce \`message\``,
  requireOneOfRoles: ['admin'],
  call: announce,
}
