const userHasRoles = require('../../utils/userHasRoles');
const removeUserFromRoles = require('../../utils/removeUserFromRoles');
const getUserByUsername = require('../../utils/getUserByUsername');
const removeUserByRoomNames = require('../../utils/removeUserByRoomNames');
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}

async function demote({ bot, message, context }) {
  const roomID = message.rid
  targetUser = await getUserByUsername(context.argumentList[0], true)

  if (message.u.username == context.argumentList[0]) {
    await bot.sendToRoom("You can't call this command on yourself!", roomID,)
    return
  }

  if (userHasRoles(targetUser, ['admin', 'Global Moderator', 'Guide'])) {
    await bot.sendToRoom(`Removing staff roles from ${targetUser.name}...`, roomID);
    targetUser = await removeUserFromRoles(targetUser, ['admin', 'Global Moderator', 'Guide']);
  } else {
    await bot.sendToRoom(`${targetUser.name} doesn't have any staff roles, but I'll double-check their channels.`, roomID);
  }

  // Kick user from any staff rooms they are in
  await bot.sendToRoom(`Removing ${targetUser.name} from all staff rooms...`, roomID);
  kickErrors = await removeUserByRoomNames(targetUser, roomLists.allStaffRooms)
  kickErrors.forEach(async (kickError) => {
    await bot.sendToRoom(kickError, context.staffLog,)
  })

  await bot.sendToRoom(`Finished! ${targetUser.name} may still be in private staff rooms that I'm not in. Remember to suspend them in the forum.`, roomID,)
}

module.exports = {
  description: "Remove a member's staff roles and kick them from staff channels.",
  help: `${process.env.ROCKETCHAT_PREFIX} demote \`username\``,
  requireOneOfRoles: ['admin'],
  call: demote,
}
