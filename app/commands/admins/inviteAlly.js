const deleteMessage = require('../../utils/deleteMessage')
const addUser = require('../../utils/addUser');
const inviteUserByRoomNames = require('../../utils/inviteUserByRoomNames');
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}

async function inviteAlly({ bot, message, context }) {
  const roomID = message.rid

  // Ensure the command is called in DMs
  if (message.roomType === 'd' || message.roomName === "invites-bot") {
    let roles = ["Non-MAP"]

    // Get the arguments
    let name = context.argumentList[0];
    let email = context.argumentList[1];
    if (context.argumentList.length >= 3 && context.argumentList[2].toLowerCase().startsWith("m")) {
      roles.push("minor")
    }

    // Create user
    await bot.sendToRoom(`Inviting ${name}`, roomID,)
    user = await addUser(name, email, false, roles)

    let inviteErrors = await inviteUserByRoomNames(user, roomLists.defaultAlly)
    inviteErrors.forEach(async (inviteError) => {
      await bot.sendToRoom(inviteError, context.staffLog,)
    })
    if (inviteErrors.length > 0) await bot.sendToRoom(`I created the user, but could not invite them to all the default ally rooms. See #${context.staffLog} for more info.`, roomID,)
    else await bot.sendToRoom(`Finished creating @${name} and inviting them to default ally rooms!`, roomID,)

  } else {
    // Delete the user's message
    await deleteMessage(roomID, message._id)
    await bot.sendToRoom(
      'For privacy reasons, this command can only be called in DMs and #invites-bot',
      roomID,
    )
    return
  }
}

module.exports = {
  description: "Add a new ally while accounting for the minor role",
  help: `${process.env.ROCKETCHAT_PREFIX} inviteAlly \`username\` \`email\` \`'minor' (optional)\``,
  requireOneOfRoles: ['admin', 'bot'],
  call: inviteAlly,
}
