const getUserByUsername = require('../../utils/getUserByUsername')
const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')
const { PublicError } = require('../../utils/errors')

async function giveRole({ bot, message, context }) {
  const roomID = message.rid

  // Get the arguments object
  let targetUser = context.argumentList[0]
  let targetRole = context.argumentList[1]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser does not already have the role
  if (!userHasRole(targetUser, targetRole)) {
    // Add the role to targetUser
    await bot.sendToRoom(
      `Giving ${targetUser.name} the ${targetRole} role...`,
      roomID,
    )
    await addUserToRole(targetUser, targetRole)
    await bot.sendToRoom(
      `Finished giving ${targetUser.name} the ${targetRole} role!`,
      roomID,
    )
    return true
  } else {
    // User already has the role, no need to do anything else
    const response = targetUser.name + ' already has the ' + targetRole + ' role'
    await bot.sendToRoom(response, roomID)
    return
  }
}

module.exports = {
  description: 'Give a member a role.',
  help: `${process.env.ROCKETCHAT_PREFIX} addRole \`username\` \`role\``,
  requireOneOfRoles: ['admin'],
  call: giveRole,
}
