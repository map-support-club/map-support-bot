const getUserByUsername = require('../../utils/getUserByUsername')
const addUserToRole = require('../../utils/addUserToRole')
const userHasRole = require('../../utils/userHasRole')
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}
const inviteUserByRoomNames = require('../../utils/inviteUserByRoomNames')
const removeUserByRoomNames = require('../../utils/removeUserByRoomNames')
const getMembersInRoom = require('../../utils/getMembersInRoom')
const { PublicError } = require('../../utils/errors')

const minorRooms = [
  'minors'
]

const roomsToRemoveFrom = [
  'onboarding-serious'
]

async function addMAP({ bot, message, context }) {
  const roomID = message.rid

  // Get the targetUser object
  let targetUserName = context.argumentList[0]
  targetUser = await getUserByUsername(targetUserName, true)
  let roomsToInvite

  // Check if the targetUser does not already have the MAP role
  if (!userHasRole(targetUser, 'MAP')) {
    // Check if the user is in prescreening and if so, fail with an error
    let prescreeners = await getMembersInRoom({
      roomName: 'pre-screening',
      count: 0,
    })
    for (member of prescreeners.members) {
      if (member.username === targetUserName) {
        throw new PublicError("Users who have not passed prescreening cannot be MAPped!")
      }
    }
    // Add the MAP role to targetUser
    await bot.sendToRoom(
      'Giving ' + targetUser.name + ' the MAP role...',
      roomID,
    )
    await addUserToRole(targetUser, 'MAP')
    roomsToInvite = roomLists.MAPRole
  } else {
    // If the user has the MAP role
    await bot.sendToRoom(
      targetUser.name + " already has the MAP role. I'll go ahead and re-add them to every channel a MAP should be able to access.",
      roomID,
    )
    roomsToInvite = roomLists.defaultMAP.concat(roomLists.onboardingMAP.concat(roomLists.MAPRole));
  }

  // Remove targetUser from removalChannels
  await bot.sendToRoom('Removing user from post-onboarding channel(s)', roomID)
  kickErrors = await removeUserByRoomNames(targetUser, roomsToRemoveFrom)
  kickErrors.forEach(async (kickError) => {
    await bot.sendToRoom(kickError, context.staffLog,)
  })

  if (userHasRole(targetUser, 'minor')) {
    //targetUser is a minor; add them to the #minors channel
    await bot.sendDirectToUser('Adding ' + targetUser.username + ' to #minors / private group channel(s) :hansen:', message.u.username)
    let inviteErrors = await inviteUserByRoomNames(targetUser, minorRooms)
    inviteErrors.forEach(async (inviteError) => {
      await bot.sendToRoom(inviteError, context.staffLog,)
    })
  }

  if (!userHasRole(targetUser, 'DM')) {
    await bot.sendToRoom(
      'Giving DM role to ' + targetUser.name,
      roomID,
    )
    await addUserToRole(targetUser, 'DM')
  }

  //Invite user to the list of channels
  await bot.sendToRoom(
    'Inviting ' + targetUser.name + ' to the MAP channels...',
    roomID,
  )
  let inviteErrors = await inviteUserByRoomNames(targetUser, roomsToInvite)
  inviteErrors.forEach(async (inviteError) => {
    await bot.sendToRoom(inviteError, context.staffLog,)
  })
  await bot.sendToRoom(
    'Finished inviting ' + targetUser.name + ' to the default channels!',
    roomID,
  )

  await bot.sendDirectToUser("Congratulations on receiving the MAP role! You have been added to default channels for MAPped members. You can join optional channels in the directory. Check out #information to learn about request-to-join channels.", targetUserName)

  await bot.sendToRoom(`@${targetUserName} granted the MAP role by @${message.u.username}`, context.forumQueue,)
}

module.exports = {
  description:
    'Give a member the MAP role and invite them to the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} addMAP \`username\``,
  requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
  call: addMAP,
}