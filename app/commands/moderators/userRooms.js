const getUserByUsername = require('../../utils/getUserByUsername')
const roomInfo = require('../../utils/getRoom')
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}

async function userRooms({ bot, message, context }) {
  const roomID = message.rid

  let room = await roomInfo({ roomId: roomID });
  let response = "";

  if (message.roomType === 'd' || roomLists.staffCommand.has(room.name)) {

    // Get the targetUser object
    let targetUser = context.argumentList[0]
    targetUser = await getUserByUsername(targetUser, true)
    let userRooms = targetUser.rooms.filter((room) => room !== undefined && room.t !== 'd')

    response = `${targetUser.name} is in the following rooms (this list does not include DMs)\n` + '```\n';
    for (room of userRooms) {
      response += room.name + "\n";
    }
    response += '```';
  } else {
    response = "This command must be called in a DM with the bot or a staff command channel."
  }
  await bot.sendToRoom(response, roomID)
}

module.exports = {
  description: 'Get a list of public and private channels that a member is in. Does not include DMs.',
  help: `${process.env.ROCKETCHAT_PREFIX} userRooms \`username\``,
  requireOneOfRoles: ['admin', 'Global Moderator'],
  call: userRooms
}
