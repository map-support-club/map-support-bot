const getUserByUsername = require('../../utils/getUserByUsername')
const isSubordinate = require('../../utils/isSubordinate')
const { api } = require('@rocket.chat/sdk')

async function globalMute({ bot, message, context }) {
  const roomID = message.rid

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser, true)

  // Ensure the command is being called on a subordinate
  if (!isSubordinate(targetUser, await getUserByUsername(message.u.username))) {
    await bot.sendToRoom("You can only call this command on people you outrank!", roomID,)
    return
  }

  let userRooms = targetUser.rooms.filter((room) => room.t === 'c' || room.t === 'p')

  await bot.sendToRoom(`Attempting to mute @${targetUser.username} in every channel...`, roomID)
  for (room of userRooms) {
    let result = await api.post('rooms.muteUser', {
      userId: targetUser._id,
      roomId: room.rid ?? room._id,
    })
    if (!result) bot.sendToRoom(`Failed to mute @${targetUser.username} in ${room.name}`, context.staffLog)
  }

  response = `@${targetUser.username} has been muted in every channel I can reach`;
  await bot.sendToRoom(response, roomID)
}

module.exports = {
  description: 'Mute a user in all channels that the bot can reach.',
  help: `${process.env.ROCKETCHAT_PREFIX} globalMute \`username\``,
  requireOneOfRoles: ['admin', 'Global Moderator'],
  call: globalMute
}
