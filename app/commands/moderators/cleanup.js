const dayjs = require('dayjs')
const relativeTime = require('dayjs/plugin/relativeTime')
dayjs.extend(relativeTime)

const getMembersInRoom = require('../../utils/getMembersInRoom')
const inviteUserByRoomNames = require('../../utils/inviteUserByRoomNames')
const removeUserFromAllRooms = require('../../utils/removeUserFromAllRooms')
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}
const getUserByUsername = require('../../utils/getUserByUsername')

const SHOULD_REMOVE = true

async function cleanup({ bot, message, context }) {
  const roomID = message.rid

  // Get the number of days limit
  let daysLimit = context.argumentList[0]

  let result = await getMembersInRoom({
    roomName: 'onboarding',
    count: 0,
  })
  let members = result.members
  let past = dayjs().subtract(daysLimit, 'days')

  await bot.sendToRoom(`Checking ${members.length} members in total...`, roomID)

  let progress = 0
  for (let member of members) {
    progress += 1

    if (progress === 50) {
      progress = 0
      await bot.sendToRoom(`Am still checking...`, roomID)
    }

    const user = await getUserByUsername(member.username, true)
    const { lastLogin, roles, active } = user

    // Check if user has any others roles than the 'user' role
    if ((!roles.includes("MAP") && !roles.includes("Non-MAP") && !roles.includes("DM") && !roles.includes("AWA")) && active === true) {

      if (past.isAfter(lastLogin)) {
        console.log(lastLogin)

        await bot.sendToRoom(
          `${member.name} last logged in ${dayjs(
            lastLogin,
          ).fromNow()}, so I'm moving them to default newbie channels (they'll still have access to private rooms I'm not in).`,
          roomID,
        )

        if (SHOULD_REMOVE) {
          let kickErrors = await removeUserFromAllRooms(user)
          kickErrors.forEach(async (kickError) => {
            await bot.sendToRoom(kickError, context.staffLog,)
          })

          await bot.sendToRoom(
            'Finished removing ' + member.name + ' from all the rooms! 🔨',
            roomID,
          )

          // Add the user to default channels
          let inviteErrors = await inviteUserByRoomNames(member, roomLists.defaultMAP);
          inviteErrors.forEach(async (inviteError) => {
            await bot.sendToRoom(inviteError, context.staffLog,)
          })

          await bot.sendToRoom(
            'Added ' + member.name + ' to default newbie channels.',
            roomID,
          )
        }
      }
    }
  }

  await bot.sendToRoom(`Finished!`, roomID)
  return true;
}

module.exports = {
  description:
    "Remove members with only the `user` role from onboarding, who haven't signed on in a specific time period.",
  help: `${process.env.ROCKETCHAT_PREFIX} cleanup \`days\``,
  requireOneOfRoles: ['admin', 'Global Moderator', 'bot'],
  call: cleanup,
}
