const unmap = require('./unMAP')
const unally = require('./unAlly')
const cleanup = require('./cleanup')
const userrooms = require('./userRooms')
const globalmute = require('./globalMute')
const globalunmute = require('./globalUnmute')

module.exports = {
  commands: { 'Moderators Commands': { unmap, unally, cleanup, userrooms, globalmute, globalunmute } },
}
