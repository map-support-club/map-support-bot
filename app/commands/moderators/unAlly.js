const getUserByUsername = require('../../utils/getUserByUsername')
const removeUserFromRoles = require('../../utils/removeUserFromRoles')
const userHasRole = require('../../utils/userHasRole')
const removeUserFromAllRooms = require('../../utils/removeUserFromAllRooms')
const inviteUserByRoomNames = require('../../utils/inviteUserByRoomNames')
let roomLists = null
try {
  roomLists = require('../../utils/privateUtils/roomLists')
} catch (err) {
  console.error("Failed to load private utils. Certain commands will not work.")
  console.error(err)
}
const { PublicError } = require('../../utils/errors')

async function unAlly({ bot, message, context }) {
  const roomID = message.rid

  // Get the targetUser object
  let targetUserName = context.argumentList[0]
  let targetUser = await getUserByUsername(targetUserName, true)

  // Check if the targetUser does already have the Non-MAP role
  if (!userHasRole(targetUser, 'Non-MAP')) {
    // User does not have the Non-MAP role, no need to do anything else
    throw new PublicError(targetUser.name + " does not have the Non-MAP role")
  }
  // Remove the roles from targetUser
  await bot.sendToRoom(
    `Removing roles from ${targetUser.name}...`,
    roomID,
  )
  const rolesToRemove = targetUser.roles.filter((role) => role !== 'user' && role !== 'minor' && role !== 'Non-MAP');
  if (rolesToRemove.length !== 0) {
    try {
      await removeUserFromRoles(targetUser, rolesToRemove);
    } catch (err) {
      console.error(err)
      await bot.sendToRoom(err.message, context.staffLog)
    }
  }

  await bot.sendToRoom(
    `Removing ${targetUser.name} from all rooms except direct messages, default channels, and private rooms I'm not in...`,
    roomID,
  )

  let kickErrors = await removeUserFromAllRooms(targetUser)
  kickErrors.forEach(async (kickError) => {
    await bot.sendToRoom(kickError, context.staffLog,)
  })

  // Add the user to default channels
  let inviteErrors = await inviteUserByRoomNames(targetUser, roomLists.defaultAlly);
  inviteErrors.forEach(async (inviteError) => {
    await bot.sendToRoom(inviteError, context.staffLog,)
  })

  await bot.sendToRoom(
    `Finished removing ${targetUser.name} from all rooms!`,
    roomID,
  )
}

module.exports = {
  description:
    'Remove the member from the AWA role (if applicable) and kick them to Non-MAP onboarding channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} unAlly \`username\``,
  requireOneOfRoles: ['admin', 'Global Moderator'],
  call: unAlly,
}
