const define = require('./define')

module.exports = {
  ...define, help: define.help.replace("define", "definition")
}
