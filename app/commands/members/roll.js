const { PublicError } = require("../../utils/errors")

async function roll({ bot, message, context }) {
    const roomID = message.rid

    let requestedDiceString = message.msg.substring(
        `${context.prefix} ${context.commandName} `.length
    ).toLowerCase()

    let diceNumber = 1
    let diceSize = 6

    let sender = message.u.name

    if (requestedDiceString.includes("d") === true) {
        let diceSplit = requestedDiceString.split("d", 2)
        diceNumber = parseInt(diceSplit[0])
        diceSize = Math.abs(parseInt(diceSplit[1]))
        if (isNaN(diceNumber)) throw new PublicError("Invalid number of dice!")
        if (isNaN(diceSize)) throw new PublicError("Invalid number of sides!")
    }
    if (diceNumber < 1) throw new PublicError("I can't roll fewer than 1 die!")

    if (diceNumber > 10) {
        await bot.sendToRoom("I only have 10 dice, so I'll roll them all", roomID)
        diceNumber = 10
    }

    let output = `${sender} rolls:`
    for (let index = 0; index < diceNumber; index++) {
        let rollNum = (diceSize === 0) ? 0 : Math.floor((Math.random() * diceSize) + 1)
        output += "\n:game_die: " + rollNum;
    }

    bot.sendToRoom(output + `\n(${diceNumber}d${diceSize})`, roomID)
}

module.exports = {
    description: 'Roll 1 or more dice!',
    help: `${process.env.ROCKETCHAT_PREFIX}  roll \`number of dice to roll\` d \`sides of the dice\``,
    call: roll,
}
