const animal = require('./animal')
const boop = require('./boop')
const bonk = require('./bonk')
const deactivate = require('./deactivate')
const define = require('./define')
const defin = require('./defin')
const definition = require('./definition')
const eat = require('./eat')
const feedback = require('./feedback')
const changebio = require('./changeBio')
const hug = require('./hug')
const poll = require('./poll')
const trivia = require('./trivia')
const headpat = require('./headpat')
const advice = require('./advice')
const icanhazdad = require('./icanhazdad')
const mention = require('./mention')
const staff = require('./staff')
const userinfo = require('./userInfo')
const celebrate = require('./celebrate')
const magic8ball = require('./magic8ball')
const tarot = require('./tarot')
const roll = require('./roll')
const rule = require('./rule')


module.exports = {
  commands: {
    'Member Commands': {
      animal,
      boop,
      bonk,
      hug,
      deactivate,
      define,
      definition,
      defin,
      eat,
      feedback,
      changebio,
      poll,
      trivia,
      headpat,
      advice,
      icanhazdad,
      mention,
      staff,
      userinfo,
      celebrate,
      magic8ball,
      tarot,
      roll,
      rule
    },
  },
}
