async function celebrate({ bot, message, context }) {
  const roomID = message.rid
  await bot.sendToRoom(":celebrate2:", roomID);
}

module.exports = {
  description:
    'Celebrates good times :D',
  help: `${process.env.ROCKETCHAT_PREFIX} celebrate`,
  cooldown: 30,
  call: celebrate,
}