const { PublicError } = require('../../utils/errors')
const getUserByUsername = require('../../utils/getUserByUsername')
const userHasRoles = require('../../utils/userHasRoles')
const deactivateUser = require('../../utils/deactivateUser')

async function deactivate({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  if (userHasRoles(user, ['admin', 'Global Moderator', 'Guide'])) {
    throw new PublicError("This command allows members to deactivate their own accounts. Staff should use the moderation dashboard to deactivate members.")
  }

  setTimeout(async () => { await deactivateUser(user) }, 1000 * 60)

  await bot.sendToRoom(
    'You will be logged out in 1 minute. To log back in, you will need to email moderators@mapsupport.club\nSave that email address now. It may take us several days to process reactivation requests.',
    roomID,
  )
  await bot.sendToRoom(`@${user.username} is being deactivated by their request. This may require a staff email or a community transparency post.`, context.staffLog)
}

module.exports = {
  description: 'DANGEROUS! Deactivate your own account. You will need to email us to have it reactivated, which may take several days.',
  help: `${process.env.ROCKETCHAT_PREFIX} deactivate`,
  call: deactivate,
}
