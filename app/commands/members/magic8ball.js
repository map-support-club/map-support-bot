async function magic8ball({ bot, message, context }) {
  const roomID = message.rid
  let responses = []

  if (message.roomName == "requests-for-support-r1-map" || message.roomName == "requests-for-support-r2-map") {
    await bot.sendToRoom("I can't run that command here", roomID);
    return false
  }

  if (Math.random() <= 0.15) {
    responses = [
      "Reply hazy, try again",
      "Ask again later",
      "Better not tell you now",
      "Cannot predict now",
      "Concentrate and ask again",]
  } else {
    responses = [
      "It is certain",
      "It is decidedly so",
      "Without a doubt",
      "Yes definitely",
      "You may rely on it",

      "As I see it, yes",
      "Most likely",
      "Outlook good",
      "Yes",
      "Signs point to yes",

      "Don't count on it",
      "My reply is no",
      "My sources say no",
      "Outlook not so good",
      "Very doubtful"]
  }

  const response = responses[Math.floor(Math.random() * responses.length)];
  await bot.sendToRoom(":sparkles: Accessing the future...", roomID);
  setTimeout(async () => { await bot.sendToRoom(":dabbingunicorn: Consulting the pedo gods...", roomID) }, 1000)
  setTimeout(async () => { await bot.sendToRoom(":mscnuke: Prediction complete!", roomID) }, 2000)
  setTimeout(async () => { await bot.sendToRoom(`The outcome is: *${response}*`, roomID) }, 2750)
}

module.exports = {
  description: 'Predict the future or seek guidance',
  help: `${process.env.ROCKETCHAT_PREFIX} magic8ball`,
  cooldown: 30,
  call: magic8ball,
}
