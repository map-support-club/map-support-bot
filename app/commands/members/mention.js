const { PublicError } = require('../../utils/errors')
const getUsersInRole = require('../../utils/getUsersInRole')
const roleNames = require('../../utils/roleNames')

async function mention({ bot, message, context }) {
  const roomID = message.rid

  // Get the arguments
  let targetRole = context.argumentList[0]?.toLowerCase()

  // Set up variables
  let users = []
  let messageToSend = ''

  // Was a role specified?
  if (targetRole !== undefined && !roleNames.ALL.has(targetRole)) {
    // Start checking which role was specified, then get users in that role
    if (roleNames.MOD.has(targetRole)) {
      messageToSend = `Mentioning all moderators & administrators: `
      users = [...users, ...(await getUsersInRole('Global Moderator'))]
      users = [...users, ...(await getUsersInRole('admin'))]
    } else if (roleNames.ADMIN.has(targetRole)) {
      messageToSend = `Mentioning all administrators: `
      users = (await getUsersInRole('admin'))
    } else if (roleNames.GUIDE.has(targetRole)) {
      messageToSend = `Mentioning all guides: `
      users = (await getUsersInRole('Guide'))
    } else {
      // If no valid role, tell the user
      await bot.sendToRoom(
        'You can only mention admins, mods, or guides with this command.',
        roomID,
      )
      return false
    }
  } else {
    messageToSend = `Mentioning all staff members: `
    // No role specified, mention all staff members
    users = [...users, ...(await getUsersInRole('Global Moderator'))]
    users = [...users, ...(await getUsersInRole('admin'))]
    users = [...users, ...(await getUsersInRole('Guide'))]
  }

  if (users.length === 0) {
    throw new PublicError("Failed to find any users with the requested role(s). Please @ mention staff members manually instead. View #new-member-information for a list of staff members.")
  }

  // Prepare message
  for (let user of users) {
    messageToSend = `${messageToSend} @${user.username} `
  }

  // Send the message
  await bot.sendToRoom(messageToSend, roomID)

  return true
}

module.exports = {
  description: 'Mention all staff members.',
  help: `${process.env.ROCKETCHAT_PREFIX} mention \`staff group (admin/mod/guide) (optional)\``,
  call: mention,
  cooldown: 60 * 30,
}
