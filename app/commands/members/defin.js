const define = require('./define')

async function defin({ bot, message, context }) {
  return await define.call({ bot, message, context }, 0)
}

module.exports = {
  ...define,
  description: define.description + " (allows very poor matches)",
  help: define.help.replace("define", "defin"),
  call: defin,
}
