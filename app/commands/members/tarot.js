const fetch = require('isomorphic-fetch')
const { PublicError } = require('../../utils/errors')

async function tarot({ bot, message, context }) {
  const roomID = message.rid

  if (message.roomName !== 'spam' && message.roomType !== 'd') {
    throw new PublicError('Command needs to be called in #spam or a DM')
  }

  await bot.sendToRoom("One moment while I shuffle the deck...", roomID);
  fetch('https://tarotapi.dev/api/v1/cards/random?n=3', {
    headers: { Accept: 'application/json' },
  })
    .then(async (response) => {
      if (response.status == 200) {
        return response.json()
      }
    })
    .then((reading) => {
      let count = 1;
      let cardNum;
      for (card of reading.cards) {
        switch (count) {
          case 1:
            cardNum = "first";
            break;
          case 2:
            cardNum = "second";
            break;
          case 3:
            cardNum = "third";
            break;
        }
        let rev = Math.random() < 0.5;
        let message = `Your ${cardNum} card is ${card.name} which `;
        if (rev) message += "typically"
        message += ` means ${tarotProcessor(card.meaning_up)}`
        if (rev) message += `\nHowever, in your case, it is reversed. Its reversed meaning is ${tarotProcessor(card.meaning_rev)}`;
        setTimeout(async () => {
          await bot.sendToRoom(message, roomID);
        }, 1000 + (count * 1000))
        count++;
      }
    })
}

function tarotProcessor(str) {
  return (str[0].toLowerCase() + str.slice(1));
}

module.exports = {
  description: 'Get a tarot reading from our all-knowing bot. Needs to be called in #spam or a DM',
  help: `${process.env.ROCKETCHAT_PREFIX} tarot`,
  cooldown: 20,
  call: tarot,
}
