const getUserByUsername = require('../../utils/getUserByUsername')

async function hug({ bot, message, context }) {
  const roomID = message.rid

  // Get the arguments
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Send the message
  message = await bot.sendToRoom(`_Hugs ${targetUser.name}!_ :hug:`, roomID)
}

module.exports = {
  description: 'Hug someone!',
  help: `${process.env.ROCKETCHAT_PREFIX} hug \`username\``,
  call: hug,
}
