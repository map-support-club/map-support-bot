const { PublicError } = require('../../utils/errors')
const getUserByUsername = require('../../utils/getUserByUsername')
const updateUser = require('../../utils/updateUser')

async function changeBio({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)
  const bio = message.msg.substring(
    `${context.prefix} ${context.commandName} `.length,
  )

  try {
    await updateUser(user, { bio: bio })
  } catch (err) {
    console.error(err)
    throw new PublicError("Could not update bio. Keep in mind, there is a character limit.")
  }
  await bot.sendToRoom(
    'Bio updated!',
    roomID,
  )
}

module.exports = {
  description:
    'Set the bio on your account',
  help: `${process.env.ROCKETCHAT_PREFIX} changebio \`new bio (leave blank to delete bio)\``,
  call: changeBio,
}
