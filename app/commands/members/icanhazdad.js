const fetch = require('isomorphic-fetch')

async function icanhazdad({ bot, message, context }) {
  const roomID = message.rid
  jokesites = ["https://icanhazdadjoke.com/", "https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=single"]
  jokeurl = jokesites[Math.floor(Math.random() * jokesites.length)];

  fetch(jokeurl, {
    headers: { Accept: 'application/json' },
  })
    .then(async (response) => {
      console.log(response)
      if (response.status == 200) {
        return response.json()
      }
    })
    .then(async (joke) => await bot.sendToRoom(joke.joke, roomID))

  // Send the message
  //message =

  return true
}

module.exports = {
  description: 'Get a dad joke!',
  help: `${process.env.ROCKETCHAT_PREFIX} icanhazdad`,
  cooldown: 30,
  call: icanhazdad,
}
