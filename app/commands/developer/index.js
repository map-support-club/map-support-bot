const delay = require('./delay')
const roominfo = require('./roomInfo')
const testcommand = require('./testCommand')
const testcooldown = require('./testCooldown')
const addword = require('./addWord')
const removeword = require('./removeWord')
const getword = require('./getWord')


module.exports = {
  commands: { 'Developer Commands': { delay, roominfo, testcommand, testcooldown, addword, removeword, getword } },
}
