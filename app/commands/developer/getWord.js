const { PublicError } = require("../../utils/errors")
const findWord = require("../../utils/findWord")


async function getWord({ bot, message, context }) {
  const roomID = message.rid

  let query = message.msg.substring(
    `${context.prefix} ${context.commandName} `.length,
  )

  let word = findWord(query)

  // Build and send the message
  output = `Key:\n>${word.key}\n
Name:\n>${word.name}\n
Definition:\n>${word.definition}\n
Aliases:\n>${word.aliases}`
  bot.sendToRoom(output, roomID)

  return true
}

module.exports = {
  description: "Get all of the data related to a word in the bot's dictionary",
  help: `${process.env.ROCKETCHAT_PREFIX} getWord \`key or alias\``,
  requireOneOfRoles: ['admin'],
  call: getWord,
}
