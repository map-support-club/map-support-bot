async function delay({ bot, message, context }) {
  let ms = context.argumentList[0] * 1000
  await new Promise(resolve => setTimeout(resolve, ms));
  bot.sendToRoom("Testing", message.rid)
}

module.exports = {
  description: 'Command that finishes after a set amount of time',
  help: `${process.env.ROCKETCHAT_PREFIX} delay \`time (seconds)\``,
  requireOneOfRoles: ['admin'],
  call: delay,
}
