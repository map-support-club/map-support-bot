const { PublicError } = require("../../utils/errors")
const readJSON = require("../../utils/readJSON")
const writeJSON = require("../../utils/writeJSON")

const defFile = 'app/utils/definitions.json'

async function addWord({ bot, message, context }) {
  const roomID = message.rid

  // Parse the user's input to get the data we need
  let messageArr = message.msg.split("\n", 4)
  if (messageArr.length < 3) throw new PublicError("Inproperly formatted input! Run `!map help addword` to see the correct format.")
  let key = messageArr[0].substring(
    `${context.prefix} ${context.commandName} `.length,
  ).trim().toLowerCase().replaceAll("-", " ").replaceAll("/", "")
  let name = messageArr[1].trim()
  let definition = messageArr[2].trim()
  let aliases = []
  if (messageArr[3]) aliases = messageArr[3].trim().split(",")
  aliases = aliases.map(alias => {
    return alias.trim().toLowerCase().replaceAll("-", " ").replaceAll("/", "")
  })

  let definitions
  try {
    definitions = readJSON(defFile)
  } catch (err) {
    console.error(err)
    throw new PublicError("I couldn't access the list of definitions. Staff probably need to fix me.")
  }

  // TODO: Checking and adding aliases should eventually be moved to a util that is also used by an addAlias command
  if (definitions[key]) throw new PublicError("There's already a word with that key in the dictionary!")
  if (aliases.length !== 0) { // If there are aliases declared
    // Build a list of all keys and aliases
    let termsAndAliases = new Set(Object.keys(definitions))
    for (def in definitions) {
      for (let alias of definitions[def].aliases) {
        termsAndAliases.add(alias)
      }
    }
    // Ensure the new term does not share any aliases with existing terms
    for (let alias of aliases) {
      if (termsAndAliases.has(alias)) {
        throw new PublicError("The desired alias '" + alias + "' is already used as a key or alias for another word!")
      }
    }
  }

  // Create the new term
  console.log("Adding new term '" + name + "' with key '" + key + "' to dictionary...")
  definitions[key] = { name, definition, aliases }
  writeJSON(defFile, definitions)
  bot.sendToRoom(name + " has been added to the dictionary!", roomID)

  return true
}

module.exports = {
  description: "Add a word to the bot's dictionary",
  help: `This command has required line breaks

${process.env.ROCKETCHAT_PREFIX} addWord \`key\` - What people are likely to search for when they want to find this word
\`name\` - This will show up above the definition
\`definition\`
\`alias1, alias2 (optional)\` - These are the other words users might use to search for this term. They should be comma-separated.`,
  requireOneOfRoles: ['admin'],
  call: addWord,
}
