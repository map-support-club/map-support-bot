const readJSON = require("./utils/readJSON");
const defFile = 'app/utils/definitions.json'

let definitions = readJSON(defFile)
let keys = Object.keys(definitions)
let keySet = new Set(keys)
for (word in definitions) {
    for (alias of definitions[word].aliases) {
        keys.push(alias)
        if (keySet.has(alias)) console.log(alias)
        else keySet.add(alias)
    }
}

for (let key of keys) {
    if (key.includes("/") || key.includes("/")) {
        console.log(key)
    }
}
