const { PublicError } = require("../../utils/errors")
const readJSON = require("../../utils/readJSON")
const writeJSON = require("../../utils/writeJSON")

const defFile = 'app/utils/definitions.json'

async function removeWord({ bot, message, context }) {
  const roomID = message.rid

  let key = message.msg.substring(
    `${context.prefix} ${context.commandName} `.length,
  )

  let definitions
  try {
    definitions = readJSON(defFile)
  } catch (err) {
    console.error(err)
    throw new PublicError("I couldn't access the list of definitions. Staff probably need to fix me.")
  }

  // Check all terms to see if any match the inputted key. If so, delete that term.
  for (term in definitions) {
    if (term === key) {
      console.log("Removing the term '" + term + "' from the dictionary...")
      let name = definitions[key].name
      delete definitions[key]
      writeJSON(defFile, definitions)
      bot.sendToRoom(name + " has been removed from to the dictionary!", roomID)
      return true
    }
  }

  throw new PublicError("I couldn't find a word with that key! Keep in mind, aliases don't work for this command.")
}

module.exports = {
  description: "Remove a word from the bot's dictionary. This command will completely and permanently delete the data associated with a word, so running the getWord command first is strongly recommended.",
  help: `${process.env.ROCKETCHAT_PREFIX} removeWord \`key\``,
  requireOneOfRoles: ['admin'],
  call: removeWord,
}
