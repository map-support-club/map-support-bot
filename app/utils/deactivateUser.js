const { api } = require('@rocket.chat/sdk')
const { StaffError } = require('./errors')
/**
 * Deactivates a user.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The user to be deactivated
 * @returns {Promise<boolean>} Whether or not the user was deactivated
 * @throws Throws an error if the user could not be deactivated
 */
const deactivateUser = async (user) => {
    console.log("Deactivating " + user.name + "...")
    result = await api.post('users.setActiveStatus', {
        userId: user._id,
        activeStatus: false
    })

    if (result && result.success) {
        return true
    } else {
        console.error(result)
        throw new StaffError("Could not deactivate @" + user.username + "\nMaybe they are the last owner of a room?")
    }
}

module.exports = deactivateUser