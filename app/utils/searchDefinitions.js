const levenshtein = require('js-levenshtein')
const readJSON = require('./readJSON')
const { PublicError } = require('./errors')

const defFile = 'app/utils/definitions.json'
/**
 * A definition in the definitions.json file (with an extra parameter from fuzzy matching)
 * @typedef {Object} Definition
 * @property {string} term
 * @property {string} definition
 * @property {string[]} aliases
 * @property {int} confidence
 */

/**
 * Get a list of definitions that match a user-defined query string.
 * @param {string} query - The parameter that the user entered to dictate which term they want defined
 * @param {int} minpct - The minimum percent confidence needed to justify returning a match
 * @returns {Definition[]} A list of terms matching the entered query string based on the minpct value
 */
const searchDefinitions = (query, minpct) => {
  query = query.trim().toLowerCase().replaceAll("-", " ").replaceAll("/", "")
  console.log("Searching for definitions for " + query + "...")

  // Load definitions
  let definitions
  try {
    definitions = readJSON(defFile)
  } catch (err) {
    console.error(err)
    throw new PublicError("I couldn't access the list of definitions. Staff probably need to fix me.")
  }
  let aliasMap = {}
  let keysAndAliases = []

  // If the query matches a defined term, return that term
  if (definitions[query]) return [definitions[query]]
  // Otherwise, look for a matching alias, while building the structures necessary for fuzzy matching
  // Iterate over all defined terms
  console.log("Couldn't find a direct match, checking aliases...")
  for (termKey in definitions) {
    keysAndAliases.push(termKey)
    // Iterate over the alias array for each term
    let aliases = definitions[termKey].aliases
    for (let alias of aliases) {
      // If the term has a matching alias, return the term
      if (alias == query) return [definitions[termKey]]
      else {
        aliasMap[alias] = termKey
        keysAndAliases.push(alias)
      }
    }
  }

  console.log("Couldn't find matching aliases, initiating fuzzy search...")
  // Calculate how similar the requested definition is to each definition key,
  // Using the Levenshtein algorithm.
  let keyMatches = keysAndAliases.map((key) => {
    return {
      key,
      distance: levenshtein(
        query,
        key,
      ),
    }
  })

  // Sort according to the distance
  keyMatches = keyMatches.sort(
    (a, b) => a.distance - b.distance,
  )

  let output = []
  let outputKeys = new Set()
  let matches = 0
  let keyIndex = 0
  while (matches < 3) {
    let keyMatch = keyMatches[keyIndex]
    let maxlen = Math.max(query.length, keyMatch.key.length)
    let pct = Math.round(100 - (100 * keyMatch.distance / maxlen))
    if (pct < minpct) {
      console.log("There were no more close enough matches, so I'm giving up.")
      break
    }
    termToAdd = aliasMap[keyMatch.key] ?? keyMatch.key
    if (!outputKeys.has(termToAdd)) {
      definitions[termToAdd]['confidence'] = pct
      output.push(definitions[termToAdd])
      outputKeys.add(termToAdd)
      if (keyIndex === 0 && keyMatch.distance < 5) {
        console.log("I found a match that was close enough, so I'm returning that.")
        break
      }
      matches++
    }
    keyIndex++
  }
  return output
}

module.exports = searchDefinitions