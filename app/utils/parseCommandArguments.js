let parseCommandArguments = command => {
  /*
  Given a command line string, break it into space delimited
  array of arguments while respecting double quotes and backslash.
  Allow empty strings to be returned in the array.
  Doesn't care/complain about unterminated quotes. Treats end of
  string as end of quote.
  
  Input: string
  Return: array of strings

  Credit to Puer 💚
  */
  let i

  let handleEscapeCharacter = () => {
    i = i + 1
    if (i < characters.length) {
      // no naughty out of bounds
      argument = argument + characters[i]
    }
    has_argument = true
  }

  let handleEndOfWord = () => {
    // not in a quote, break the arg
    if (has_argument) {
      arguments_.push(argument) // keep it
      argument = '' // clear it
      has_argument = false
    }
    if (characters[i + 1] && characters[i + 1] === '@') i++;
  }

  let addChar = () => {
    has_argument = true
    argument += characters[i] // add the char
  }

  command = command.trim()
  let characters = command.split('')
  let in_quote = false // am I inside a quoted arg?
  let has_argument = false // do I have an argument to add to the list?
  let argument = '' // the argument being parsed
  let arguments_ = [] // the array to return

  for (i = 0; i < characters.length; i++) {
    switch (characters[i]) {
      case '\\': // take the next character as-is
        handleEscapeCharacter()
        break

      case '"': // begin or end a quoted arg
      case '“':
      case '”':
        in_quote = !in_quote
        has_argument = true // allow empty string ("") arguments
        break

      case '@':
        if (i === 0 && !in_quote) {
          break
        }
        else {
          addChar()
          break
        }

      case ' ': // whitespace
      case '\t': // tab
      case '\r': // carriage return
      case '\n': // line feed
        if (in_quote) {
          addChar()
          break
        } else {
          handleEndOfWord()
          break
        }

      default:
        addChar()
    }
  }
  if (has_argument) {
    // keep any final argument
    arguments_.push(argument)
  }
  return arguments_
}

module.exports = parseCommandArguments
