const getRoom = require('./getRoom')

// Not used anywhere, candidate for deletion
const getRooms = async (rooms) => {
  let _rooms = []
  for (let room of rooms) {
    _rooms.push(await getRoom(room))
  }
  return _rooms
}

module.exports = getRooms
