const { api } = require('@rocket.chat/sdk');
const { PublicError } = require('./errors');
const { defaults } = require('@rocket.chat/sdk/dist/lib/methodCache');

/**
 * Creates a new user while automatically handling all necessary settings.
 * @param {string} username - The desired username of the new user
 * @param {string} email - The email address for the new user's account
 * @param {boolean} defaults - Whether or not to add the user to the default (MAP) channels
 * @param {string[]} roles - The roles, in addition to the user role, that the user should have
 * @returns {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} The new user
 * @throws Throws an error if the user could not be created or an API error occurred
 */
const addUser = async (username, email, defaults, roles = []) => {
  console.log("Creating " + username + "...")

  // Set up roles
  roles.push("user")
  console.log("ADDING WITH ROLES ", roles)

  // Create user
  let result = await api.post('users.create', {
    email: email,
    name: username,
    username: username,
    password: "",
    roles: roles,
    setRandomPassword: true,
    joinDefaultChannels: defaults,
    sendWelcomeEmail: true
  })

  if (result && result.success) {
    return result.user
  } else {
    console.error(result)
    throw new PublicError("Could not create " + username + ". Please create manually to see details.")
  }
}

module.exports = addUser