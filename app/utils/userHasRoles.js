const userHasRole = require('./userHasRole')

// Note that this command only checks whether a user has one of the roles, not all of them
const userHasRoles = (user, roles = []) => {
  for (let role of roles) {
    if (userHasRole(user, role)) {
      return true
    }
  }
  return false
}

module.exports = userHasRoles
