const { api } = require('@rocket.chat/sdk')
const { PublicError } = require('./errors')

/**
 * Add a user to a role.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The user receiving the role
 * @param {string} roleName - The name of the role being assigned
 * @param {string} roomId - The ID of the room where the user should have this role. Defaults to undefined.
 * @returns The role that the user was assigned
 * @throws Throws an error if the role could not be assigned or if an API error occurred
 */
const addUserToRole = async (user, roleName, roomId = undefined) => {
  console.log("Giving " + user.name + " the " + roleName + " role...")
  let result = await api.post('roles.addUserToRole', {
    roleName,
    username: user.username,
    roomId,
  })
  if (result && result.success) {
    return result.role
  } else {
    console.error(result)
    throw new PublicError("Failed to give " + user.name + " the " + roleName + " role")
  }
}

module.exports = addUserToRole
