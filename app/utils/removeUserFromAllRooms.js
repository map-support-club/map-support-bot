const getUserByUsername = require('./getUserByUsername');
const removeUserFromRoom = require('./removeUserFromRoom')

/**
 * Removes a user from every room the bot can reach. Will not affect DMs or private rooms that the bot is not a member of.
 * @summary Removes a user from every room the bot can reach. Will not affect DMs or private rooms that the bot is not a member of.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI & {rooms: import('@rocket.chat/sdk/dist/utils/interfaces').IRoomAPI[]}} user - The user who is being removed from all rooms
 * @returns {string[]} A list of failures that occurred while the function was running
 */
const removeUserFromAllRooms = async (user) => {
  console.log("Removing " + user.name + " from all rooms...");
  return await removeUserFromRooms(user, user.rooms);
}

/**
 * Removes a user from all of the rooms in a list.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The user to be removed from the rooms
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IRoomAPI[]} rooms - The list of rooms to remove the user from
 * @param {boolean} onlyUserRooms - Indicates whether the list of rooms has been filtered to only include rooms that the user is in. This should be left as false unless set to true by another util.
 * @returns {string[]} A list of errors encountered while removing users from rooms
 */
const removeUserFromRooms = async (user, rooms) => {
  // Figure out which rooms the bot is in
  bot = await getUserByUsername(process.env.ROCKETCHAT_USER, true)
  botRooms = new Set(bot.rooms.map(room => { return room.name }))

  rooms = rooms.filter((room) => room.t === 'c' || room.t === 'p')
  let errors = []
  for (let room of rooms) {
    if (room == undefined) {
      console.log("Skipped removing " + user.name + " from an undefined room.")
    } else if (room.t === "p" && !botRooms.has(room.name)) {
      console.log("Skipped removing " + user.name + " from #" + room.name + " since it's a private room I'm not in.")
    } else {
      try {
        await removeUserFromRoom(user, room)
      } catch (err) {
        console.error(err)
        errors.push("Error while removing " + user.name + " from #" + room.name + ": " + err.message)
      }
    }
  }
  return errors
}

module.exports = removeUserFromAllRooms
