const { api } = require('@rocket.chat/sdk')
const { StaffError } = require('./errors')

/**
 * Removes a user from a room. Intended to be used by other utils, not recommend for command-level code. Consider removeUserFromRooms or removeUserByRoomName instead.
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IUserAPI} user - The user to remove from the room
 * @param {import('@rocket.chat/sdk/dist/utils/interfaces').IRoomAPI} room - The room to remove the user from
 * @returns {Promise<import('@rocket.chat/sdk/dist/utils/interfaces').IRoomAPI>} The room that the user was removed from
 * @throws Throws an error if the user could not be removed from a room (including private rooms that the bot is not in) or an API error occurred
 */
const removeUserFromRoom = async (user, room) => {
    console.log("Removing " + user.name + " from " + room.name + "...")
    let roomId = room.rid ?? room._id
    let result
    if (room.t === 'c') {
        result = await api.post('channels.kick', {
            roomId: roomId,
            userId: user._id,
        })
    } else if (room.t === 'p') {
        result = await api.post('groups.kick', {
            roomId: roomId,
            userId: user._id,
        })
    } else {
        throw new StaffError("#" + room.name + " has invalid type " + room.t)
    }
    if (result && result.success) {
        return result.channel ?? result.group
    } else {
        console.error(result)
        throw new StaffError("Failed to remove " + user.name + " from #" + room.name)
    }
}

module.exports = removeUserFromRoom